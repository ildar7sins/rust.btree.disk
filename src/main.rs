mod commands;
mod node;
mod page;
mod error;
mod node_type;
mod page_layout;
mod btree;
mod pager;
mod wal;

use std::env;
use std::path::Path;
use crate::btree::BTreeBuilder;
use crate::commands::get_command;
use crate::node_type::KeyValuePair;

use once_cell::sync::Lazy;

fn main() {
    let args: Vec<String> = env::args().collect();

    let db_name = args.get(1).unwrap_or(&String::from("")).clone();
    let index = args.get(2).unwrap_or(&String::from("")).clone();
    let command = args.get(3).unwrap_or(&String::from("")).clone();
    let key = args.get(4).unwrap_or(&String::from("")).clone();
    let value = args.get(5).unwrap_or(&String::from("")).clone();

    println!("{:?}", db_name);
    println!("{:?}", index);
    println!("{:?}", command);
    println!("{:?}", key);
    println!("{:?}", value);
    println!("{:?}", get_command(&command));

    let path: String = format!("./db/{}/{}", db_name, index).clone();

    let btree = BTreeBuilder::new()
        .path(path)
        .b_parameter(2)
        .build();

    let mut res = btree.unwrap();

    let _insert_1 = res.insert(KeyValuePair::new("f".to_string(), "shalom".to_string()));
    let _insert_2 = res.insert(KeyValuePair::new("a".to_string(), "shalom".to_string()));
    let _insert_3 = res.insert(KeyValuePair::new("c".to_string(), "shalom".to_string()));

    let search = res.search("a".to_string());

    println!("{:#?}", search);
}
