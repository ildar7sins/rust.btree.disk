
#[derive(Debug)]
pub enum Command {
    SET,
    GET,
    DELETE,
    UPDATE,

    #[allow(non_camel_case_types)]
    CREATE_INDEX,
    #[allow(non_camel_case_types)]
    DELETE_INDEX,
}

pub fn get_command(command: &String) -> Result<Command, &'static str>
{
    let check = command.to_lowercase();

    match check.as_str() {
        "set" => Ok(Command::SET),
        "get" => Ok(Command::GET),
        "delete" => Ok(Command::DELETE),
        "update" => Ok(Command::UPDATE),

        "create_index" => Ok(Command::CREATE_INDEX),
        "delete_index" => Ok(Command::DELETE_INDEX),
        _ => Err("Unknown command")
    }
}